#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f1xx_hal.h"

#define ONBOARD_LED_Pin GPIO_PIN_13
#define ONBOARD_LED_GPIO_Port GPIOC
#define LEFT_THRUSTER_POWER_DOWN_Pin GPIO_PIN_0
#define LEFT_THRUSTER_POWER_DOWN_GPIO_Port GPIOA
#define LEFT_THRUSTER_POWER_UP_Pin GPIO_PIN_1
#define LEFT_THRUSTER_POWER_UP_GPIO_Port GPIOA
#define DIVING_SERVO_DOWN_Pin GPIO_PIN_2
#define DIVING_SERVO_DOWN_GPIO_Port GPIOA
#define DIVING_SERVO_UP_Pin GPIO_PIN_3
#define DIVING_SERVO_UP_GPIO_Port GPIOA
#define RIGHT_THRUSTER_POWER_DOWN_Pin GPIO_PIN_4
#define RIGHT_THRUSTER_POWER_DOWN_GPIO_Port GPIOA
#define RIGHT_THRUSTER_POWER_UP_Pin GPIO_PIN_5
#define RIGHT_THRUSTER_POWER_UP_GPIO_Port GPIOA
#define RF_D11_Pin GPIO_PIN_12
#define RF_D11_GPIO_Port GPIOB
#define RF_D12_Pin GPIO_PIN_13
#define RF_D12_GPIO_Port GPIOB
#define RF_D13_Pin GPIO_PIN_14
#define RF_D13_GPIO_Port GPIOB

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
