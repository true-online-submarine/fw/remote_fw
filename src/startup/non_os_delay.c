#include "non_os_delay.h"
#include "main.h"
#include "stm32f1xx_it.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "peripheral_init.h"

extern TIM_HandleTypeDef htim3;

#define NO_OS_DELAY_USE_IRQ 1
#define NO_OS_DELAY_DONT_USE_IRQ 0

void no_os_delay_timer_init_irq(void)
{
    tim3_counter_init(NO_OS_DELAY_USE_IRQ);
}

void no_os_delay_timer_disable_irq(void)
{
    tim3_counter_init(NO_OS_DELAY_DONT_USE_IRQ);
}

void no_os_delay_us(uint32_t us)
{
    TIM3->CNT = 0;
    while (TIM3->CNT < us);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    if (htim->Instance == TIM3) {
        HAL_IncTick();
    }
}
