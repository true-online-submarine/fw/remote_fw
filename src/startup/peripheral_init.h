#ifndef __PERIPHERAL_INIT_H
#define __PERIPHERAL_INIT_H

#ifdef __cplusplus
extern "C" {
#endif

void clock_init(void);
void gpio_init(void);
void uart1_init(void);
void adc1_init(void);
void spi1_init(void);
void tim3_counter_init(int use_irq);
void systick_disable_int(void);

#ifdef __cplusplus
}
#endif

#endif /* __PERIPHERAL_INIT_H */
