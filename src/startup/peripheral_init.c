#include "main.h"
#include "peripheral_init.h"
#include "sub_errors.h"

#include "FreeRTOSConfig.h"

ADC_HandleTypeDef hadc1;
SPI_HandleTypeDef hspi1;
UART_HandleTypeDef huart1;
TIM_HandleTypeDef htim3;

void clock_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
    RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
    RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

    /* Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure. */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_CLOCK_ASSERT_10000, SUB_ASSERT_CRITICAL);

    /* Initializes the CPU, AHB and APB buses clocks */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_CLOCK_ASSERT_10001, SUB_ASSERT_CRITICAL);

    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
    PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
    ret = HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_CLOCK_ASSERT_10002, SUB_ASSERT_CRITICAL);
}

void adc1_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    ADC_ChannelConfTypeDef sConfig = { 0 };

    /* Common config */
    hadc1.Instance = ADC1;
    hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
    hadc1.Init.ContinuousConvMode = DISABLE;
    hadc1.Init.DiscontinuousConvMode = DISABLE;
    hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
    hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    hadc1.Init.NbrOfConversion = 1;
    ret = HAL_ADC_Init(&hadc1);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_ADC1_ASSERT_10005, SUB_ASSERT_CRITICAL);

    /* Configure Regular Channel */
    sConfig.Channel = ADC_CHANNEL_6;
    sConfig.Rank = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
    ret = HAL_ADC_ConfigChannel(&hadc1, &sConfig);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_ADC1_ASSERT_10006, SUB_ASSERT_CRITICAL);
}

void spi1_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;
    hspi1.Instance = SPI1;
    hspi1.Init.Mode = SPI_MODE_MASTER;
    hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
    hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
    hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
    hspi1.Init.NSS = SPI_NSS_SOFT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
    hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
    hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi1.Init.CRCPolynomial = 10;
    ret = HAL_SPI_Init(&hspi1);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_SPI1_ASSERT_10010, SUB_ASSERT_CRITICAL);
}

void uart1_init(void)
{
    HAL_StatusTypeDef ret = HAL_OK;

    huart1.Instance = USART1;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    ret = HAL_UART_Init(&huart1);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_UART1_ASSERT_10015, SUB_ASSERT_CRITICAL);
}

void gpio_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, RF_D11_Pin|RF_D12_Pin|RF_D13_Pin|GPIO_PIN_7, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_RESET);

    /*Configure GPIO pin : ONBOARD_LED_Pin */
    GPIO_InitStruct.Pin = ONBOARD_LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ONBOARD_LED_GPIO_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : LEFT_THRUSTER_POWER_DOWN_Pin LEFT_THRUSTER_POWER_UP_Pin DIVING_SERVO_DOWN_Pin DIVING_SERVO_UP_Pin
                            RIGHT_THRUSTER_POWER_DOWN_Pin RIGHT_THRUSTER_POWER_UP_Pin */
    GPIO_InitStruct.Pin = LEFT_THRUSTER_POWER_DOWN_Pin|LEFT_THRUSTER_POWER_UP_Pin|DIVING_SERVO_DOWN_Pin|DIVING_SERVO_UP_Pin
                            |RIGHT_THRUSTER_POWER_DOWN_Pin|RIGHT_THRUSTER_POWER_UP_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : RF_D11_Pin RF_D12_Pin RF_D13_Pin PB7 */
    GPIO_InitStruct.Pin = RF_D11_Pin|RF_D12_Pin|RF_D13_Pin|GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pin : PA15 */
    GPIO_InitStruct.Pin = GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : PB6 */
    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void tim3_counter_init(int use_irq)
{
    HAL_StatusTypeDef ret = HAL_OK;

    __HAL_RCC_TIM3_CLK_ENABLE();

    /* disable timer */
    TIM3->DIER &= ~(TIM_IT_UPDATE);
    TIM3->CR1 &= ~(TIM_CR1_CEN);

    htim3.Instance = TIM3;
    htim3.Init.Prescaler = 72 - 1;
    htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim3.Init.Period = use_irq ? 1000 : 65535; // 1 ms interrupt or just max counter to do less 1ms delays
    htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    ret = HAL_TIM_Base_Init(&htim3);
    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_TIM3_ASSERT_10020, SUB_ASSERT_CRITICAL);

    if (use_irq) {
        /* This timer is used to generate 1ms for HAL_Delay().
        For this we need the interrupt enabled. Therefore TIM3_IRQn priority must be more
        than masked by Freertos.
        
        (FreeRTOS maSkes interrupts lower then
        configMAX_SYSCALL_INTERRUPT_PRIORITY before the scheduler starts)!!
        Actually RTOS don't UNMASK previously masked IRQ.

        For example we called xTaskCreateStatic() first time.
        uxCriticalNesting variable hasn't been initialized yet. It is done by the xPortStartScheduler().
        uxCriticalNesting is static variable which value is 0xaaaaaaaa.
        Therefore function vPortExitCritical() believes that we have nested calls for entering
        ctirical sectoin. Might be it is a BUG. Might be not. Anyway scheduler initialize the
        variable during start. But for know we must remember that if we want use interrupts
        before OS start (f.i. to implement increment for HAL_Delay function) we MUST
        use priority larger than interrupt's priority masked by OS.

         ! For Cortex-M3 the lower number the larger proirity !
        */

        HAL_NVIC_SetPriority(TIM3_IRQn, (configMAX_SYSCALL_INTERRUPT_PRIORITY >> __NVIC_PRIO_BITS) - 1, 0);
        HAL_NVIC_EnableIRQ(TIM3_IRQn);

        ret = HAL_TIM_Base_Start_IT(&htim3);
    } else {
        HAL_NVIC_DisableIRQ(TIM3_IRQn);

        ret = HAL_TIM_Base_Start(&htim3);
    }

    SUB_ASSERT(ret != HAL_OK, REMOTE_INIT_TIM3_ASSERT_10021, SUB_ASSERT_CRITICAL);
}

void systick_disable_int(void)
{
    SysTick->CTRL &= ~(SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk| SysTick_CTRL_ENABLE_Msk);
}
