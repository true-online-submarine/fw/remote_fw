#include "main.h"
#include "peripheral_init.h"
#include "sub_logger.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "non_os_delay.h"

#include "push_buttons_thread.h"

int main(void)
{
    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    clock_init();

    /* FreeRTOS will setup SysTick by itself.
       We get HardFault when SysTick IRQ has happened before the scheduler started.
       So, we MUST disable it. Otherwise 1ms probably elapsed if we use uart logger
       function for instance and we got HF. */
    systick_disable_int();

    /* No OS delay to make HAL_Delay() alive and init counter for 1ms less delays */
    no_os_delay_timer_init_irq();

    gpio_init();
    uart1_init();
    adc1_init();
    spi1_init();

    /* RTOS thread to handle buttons and its callbacks */
    push_button_thread_create();

    /* This "Hello" from the FW means that all necessary initializations successfully done.
       Otherwise we got some messages from SUB_ASSERT() and than system reset.
       Check the sub_error.h module in common repo. (This module is included by PlatformIO) */
    logger_dgb_print("[Remote] FW started. CPU clock = %d Hz\n", SystemCoreClock);

    /* Disable 1ms IRQ for hal_delay, because after OS start, we use vApplicationTickHook() for inc tick */
    no_os_delay_timer_disable_irq();

    /* Start the scheduler. */
    vTaskStartScheduler();

    /* Will only get here if there was insufficient memory to create the idle
    task. The idle task is created within vTaskStartScheduler(). */
    for( ;; ) {
        logger_dgb_print("[Remote ERR] insufficient memory!\n");
        HAL_GPIO_TogglePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin);
        HAL_Delay(100);
    }
}
