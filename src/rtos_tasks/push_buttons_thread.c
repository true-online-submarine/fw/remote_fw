#include <stdlib.h>

#include "main.h"
#include "push_buttons_thread.h"

#include "FreeRTOS.h"
#include "task.h"

#include "push_buttons.h"
#include "push_buttons_config.h"
#include "sub_errors.h"

#include "pt2262.h"

static StaticTask_t btn_task_tcb;
static StackType_t btn_task_stack[configMINIMAL_STACK_SIZE];
static void push_button_thread(void *params)
{
    for(;;) {
        vTaskDelay(20 / portTICK_PERIOD_MS);
        push_buttons_proc();
    }
}

static void pt2262_data_tx(void *d)
{
    pt2262_tx_cmd(d);

    /* Visial comfirmation that pins have been setup */
    HAL_GPIO_TogglePin(ONBOARD_LED_GPIO_Port, ONBOARD_LED_Pin);
}

void push_button_thread_create(void)
{
    TaskHandle_t h;

    /* https://stackoverflow.com/questions/1810083/c-pointers-pointing-to-an-array-of-fixed-size */
    static uint8_t left_thruster_slower_cmd[] = PT2262_TX_CMD_LEFT_THRUSTER_POWER_DOWN;
    static uint8_t left_thruster_faster_cmd[] = PT2262_TX_CMD_LEFT_THRUSTER_POWER_UP;
    static uint8_t diving_cmd[] = PT2262_TX_CMD_GO_DOWN;
    static uint8_t surfacing_cmd[] = PT2262_TX_CMD_GO_UP;
    static uint8_t right_thruster_slower_cmd[] = PT2262_TX_CMD_RIGHT_THRUSTER_POWER_DOWN;
    static uint8_t right_thruster_faster_cmd[] = PT2262_TX_CMD_RIGHT_THRUSTER_POWER_UP;
    static uint8_t neutral_cmd[] = PT2262_TX_CMD_NEUTRAL;

    push_button_set_pressed_cb(PUSH_BTN_1_ID, pt2262_data_tx, left_thruster_slower_cmd);
    push_button_set_pressed_cb(PUSH_BTN_2_ID, pt2262_data_tx, left_thruster_faster_cmd);
    push_button_set_pressed_cb(PUSH_BTN_3_ID, pt2262_data_tx, diving_cmd);
    push_button_set_pressed_cb(PUSH_BTN_4_ID, pt2262_data_tx, surfacing_cmd);
    push_button_set_pressed_cb(PUSH_BTN_5_ID, pt2262_data_tx, right_thruster_slower_cmd);
    push_button_set_pressed_cb(PUSH_BTN_6_ID, pt2262_data_tx, right_thruster_faster_cmd);

    push_button_set_released_cb(PUSH_BTN_1_ID, pt2262_data_tx, neutral_cmd);
    push_button_set_released_cb(PUSH_BTN_2_ID, pt2262_data_tx, neutral_cmd);
    push_button_set_released_cb(PUSH_BTN_3_ID, pt2262_data_tx, neutral_cmd);
    push_button_set_released_cb(PUSH_BTN_4_ID, pt2262_data_tx, neutral_cmd);
    push_button_set_released_cb(PUSH_BTN_5_ID, pt2262_data_tx, neutral_cmd);
    push_button_set_released_cb(PUSH_BTN_6_ID, pt2262_data_tx, neutral_cmd);

    h = xTaskCreateStatic(push_button_thread, "buttons", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, btn_task_stack, &btn_task_tcb);
    SUB_ASSERT(h == NULL, REMOTE_BUTTON_THREAD_ASSERT_10065, SUB_ASSERT_CRITICAL);
}
