#ifndef __PUSH_BUTTONS_THREAD_H
#define __PUSH_BUTTONS_THREAD_H

#ifdef __cplusplus
extern "C" {
#endif

void push_button_thread_create(void);

#ifdef __cplusplus
}
#endif

#endif /* __PUSH_BUTTONS_THREAD_H */
