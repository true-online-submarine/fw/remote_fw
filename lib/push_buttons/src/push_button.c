#include "push_buttons.h"
#include "push_buttons_config.h"

#define DEBOUNCE_CNT 5

static push_button_t btns_array[PUSH_BTN_CNT] = {
    [PUSH_BTN_1_ID] = { PUSH_BTN_1_ID, PUSH_BTN_1_PORT, PUSH_BTN_1_PIN, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
    [PUSH_BTN_2_ID] = { PUSH_BTN_2_ID, PUSH_BTN_2_PORT, PUSH_BTN_2_PIN, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
    [PUSH_BTN_3_ID] = { PUSH_BTN_3_ID, PUSH_BTN_3_PORT, PUSH_BTN_3_PIN, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
    [PUSH_BTN_4_ID] = { PUSH_BTN_4_ID, PUSH_BTN_4_PORT, PUSH_BTN_4_PIN, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
    [PUSH_BTN_5_ID] = { PUSH_BTN_5_ID, PUSH_BTN_5_PORT, PUSH_BTN_5_PIN, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
    [PUSH_BTN_6_ID] = { PUSH_BTN_6_ID, PUSH_BTN_6_PORT, PUSH_BTN_6_PIN, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL,},
};

void push_buttons_proc(void)
{
    for (uint32_t i = 0; i < PUSH_BTN_CNT; ++i) {
        if ((push_btn_lib_read_pin(btns_array[i].gpio_port, btns_array[i].gpio_pin) == PUSH_BTN_RESET) \
             && (!btns_array[i].is_pressed)) {

            ++btns_array[i].debounce_cnt;
            if (btns_array[i].debounce_cnt < DEBOUNCE_CNT) {
                continue;
            }

            btns_array[i].is_pressed = 1;
            btns_array[i].debounce_cnt = 0;
            if (btns_array[i].pressed_cb) {
                btns_array[i].pressed_cb(btns_array[i].pressed_data);
#if defined(BUTTONS_LIB_USE_LOGGER)
                push_btn_lib_logger("btn[%d] pressed\n", i);
#endif
            }
        } else if ((push_btn_lib_read_pin(btns_array[i].gpio_port, btns_array[i].gpio_pin) == PUSH_BTN_SET) && \
                   (btns_array[i].is_pressed)) {
            btns_array[i].is_pressed = 0;
            btns_array[i].debounce_cnt = 0;
            if (btns_array[i].released_cb) {
                btns_array[i].released_cb(btns_array[i].released_data);
#if defined(BUTTONS_LIB_USE_LOGGER)
                push_btn_lib_logger("btn[%d] released\n", i);
#endif
            }

            if (btns_array[i].clicked_cb) {
                btns_array[i].clicked_cb(btns_array[i].clicked_data);
#if defined(BUTTONS_LIB_USE_LOGGER)
                push_btn_lib_logger("btn[%d] clicked\n", i);
#endif
            }
        } else {
            btns_array[i].debounce_cnt = 0;
        }

        /* SW clicked buttons. Need for remote control or tests */
        if (btns_array[i].is_simulate_click) {
            btns_array[i].is_simulate_click = 0;

            if (btns_array[i].clicked_cb) {
                btns_array[i].clicked_cb(btns_array[i].clicked_data);
#if defined(BUTTONS_LIB_USE_LOGGER)
                push_btn_lib_read_pin("btn[%d] SW clicked\n", i);
#endif
            }
        }
    }
}

void push_button_set_pressed_cb(unsigned int btn_id, void (*pressed_cb)(void *), void *data)
{
    if ((btn_id < 0) || (btn_id >= PUSH_BTN_CNT)) {
        return;
    }

    btns_array[btn_id].pressed_cb = pressed_cb;
    btns_array[btn_id].pressed_data = data;
}

void push_button_set_released_cb(unsigned int btn_id, void (*released_cb)(void *), void *data)
{
    if ((btn_id < 0) || (btn_id >= PUSH_BTN_CNT)) {
        return;
    }

    btns_array[btn_id].released_cb = released_cb;
    btns_array[btn_id].released_data = data;
}

void push_button_set_clicked_cb(unsigned int btn_id, void (*clicked_cb)(void *), void *data)
{
    if ((btn_id < 0) || (btn_id >= PUSH_BTN_CNT)) {
        return;
    }

    btns_array[btn_id].clicked_cb = clicked_cb;
    btns_array[btn_id].clicked_data = data;
}

void push_button_simulate_click(unsigned int btn_id)
{
    btns_array[btn_id].is_simulate_click = 1;
}
