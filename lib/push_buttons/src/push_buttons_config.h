#ifndef __PUSH_BUTTONS_CONFIG_H
#define __PUSH_BUTTONS_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "sub_logger.h"

#define PUSH_BTN_CNT 6 /* It is equivalent button's IDs from 0 to (PUSH_BTN_CNT - 1) */
#define PUSH_BTN_RESET GPIO_PIN_RESET
#define PUSH_BTN_SET GPIO_PIN_SET

#define PUSH_BTN_1_ID   0
#define PUSH_BTN_1_PORT (uint32_t)LEFT_THRUSTER_POWER_DOWN_GPIO_Port
#define PUSH_BTN_1_PIN  (uint32_t)LEFT_THRUSTER_POWER_DOWN_Pin

#define PUSH_BTN_2_ID   1
#define PUSH_BTN_2_PORT (uint32_t)LEFT_THRUSTER_POWER_UP_GPIO_Port
#define PUSH_BTN_2_PIN  (uint32_t)LEFT_THRUSTER_POWER_UP_Pin

#define PUSH_BTN_3_ID   2
#define PUSH_BTN_3_PORT (uint32_t)DIVING_SERVO_DOWN_GPIO_Port
#define PUSH_BTN_3_PIN  (uint32_t)DIVING_SERVO_DOWN_Pin

#define PUSH_BTN_4_ID   3
#define PUSH_BTN_4_PORT (uint32_t)DIVING_SERVO_UP_GPIO_Port
#define PUSH_BTN_4_PIN  (uint32_t)DIVING_SERVO_UP_Pin

#define PUSH_BTN_5_ID   4
#define PUSH_BTN_5_PORT (uint32_t)RIGHT_THRUSTER_POWER_DOWN_GPIO_Port
#define PUSH_BTN_5_PIN  (uint32_t)RIGHT_THRUSTER_POWER_DOWN_Pin

#define PUSH_BTN_6_ID   5
#define PUSH_BTN_6_PORT (uint32_t)RIGHT_THRUSTER_POWER_UP_GPIO_Port
#define PUSH_BTN_6_PIN  (uint32_t)RIGHT_THRUSTER_POWER_UP_Pin

#if defined(BUTTONS_LIB_USE_LOGGER)
# define push_btn_lib_logger(...) logger_dgb_print(__VA_ARGS__)

#  if !defined(push_btn_lib_logger)
#  error "Define push_btn_lib_logger() function for buttons lib or disable BUTTONS_LIB_USE_LOGGER!"
#  endif
#endif


#define push_btn_lib_read_pin(port, pin) HAL_GPIO_ReadPin((GPIO_TypeDef *)port, (uint16_t)pin)
#if !defined(push_btn_lib_read_pin)
# error "Define push_btn_lib_read_pin() function for buttons lib!"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __PUSH_BUTTONS_CONFIG_H */
