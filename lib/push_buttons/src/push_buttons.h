#ifndef __PUSH_BUTTON_H
#define __PUSH_BUTTON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef struct
{
    uint32_t id;
    uint32_t gpio_port;
    uint32_t gpio_pin;
    uint32_t is_pressed;
    uint32_t pressed_time; /* Not implemented for now */
    uint32_t is_simulate_click;
    uint32_t debounce_cnt;

    void (*pressed_cb)(void *);
    void (*released_cb)(void *);
    void (*clicked_cb)(void *);

    void *pressed_data;
    void *released_data;
    void *clicked_data;
} push_button_t;

void push_buttons_proc(void);
void push_button_set_pressed_cb(unsigned int btn_id, void (*pressed_cb)(void *), void *data);
void push_button_set_released_cb(unsigned int btn_id, void (*released_cb)(void *), void *data);
void push_button_set_clicked_cb(unsigned int btn_id, void (*clicked_cb)(void *), void *data);
void push_button_simulate_click(unsigned int btn_id);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif  /* __PUSH_BUTTON_H */
