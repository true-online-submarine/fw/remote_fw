#ifndef __PT2262_RF433_TRANSMITTER_H
#define __PT2262_RF433_TRANSMITTER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "pt2262_config.h"

#define PT2262_TX_CMD_NEUTRAL { 0, 0, 0 }

/* Reduce/increase speed of LEFT engine for several percens */
#define PT2262_TX_CMD_LEFT_THRUSTER_POWER_DOWN { 0, 0, 1 }
#define PT2262_TX_CMD_LEFT_THRUSTER_POWER_UP { 0, 1, 0 }

/* Diving/Surfacing commands for syringe diving system */
#define PT2262_TX_CMD_GO_DOWN { 0, 1, 1 }
#define PT2262_TX_CMD_GO_UP { 1, 0, 0 }

/* Reduce/increase speed of RIGHT engine for several percens */
#define PT2262_TX_CMD_RIGHT_THRUSTER_POWER_DOWN { 1, 0, 1 }
#define PT2262_TX_CMD_RIGHT_THRUSTER_POWER_UP { 1, 1, 0 }

void pt2262_tx_cmd(uint8_t bits[PT2262_DATA_PINS_IN_USED]);

#ifdef __cplusplus
}
#endif

#endif /* __PT2262_RF433_TRANSMITTER_H */
