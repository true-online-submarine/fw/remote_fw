#include "pt2262.h"
#include "pt2262_config.h"

#include "sub_logger.h"

void pt2262_tx_cmd(uint8_t bits[PT2262_DATA_PINS_IN_USED])
{
    pt2262_lib_set_pin(PT2262_DATA_BIT_0_PORT, PT2262_DATA_BIT_0_PIN, bits[PT2262_DATA_BIT_0_ID]);
    pt2262_lib_set_pin(PT2262_DATA_BIT_1_PORT, PT2262_DATA_BIT_1_PIN, bits[PT2262_DATA_BIT_1_ID]);
    pt2262_lib_set_pin(PT2262_DATA_BIT_2_PORT, PT2262_DATA_BIT_2_PIN, bits[PT2262_DATA_BIT_2_ID]);
}
