#ifndef __PT2262_RF433_TRANSMITTER_CONFIG_H
#define __PT2262_RF433_TRANSMITTER_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"

/* Address bits will be transmitted by the radio module itself */
#define PT2262_ADDRESS_BITS { PT2262_PIN_LOW_STATE,    \
                              PT2262_PIN_LOW_STATE,    \
                              PT2262_PIN_HIGH_STATE,   \
                              PT2262_PIN_FLOAT_STATE,  \
                              PT2262_PIN_HIGH_STATE,   \
                              PT2262_PIN_FLOAT_STATE,  \
                              PT2262_PIN_LOW_STATE,    \
                              PT2262_PIN_LOW_STATE,    \
                              PT2262_PIN_HIGH_STATE }

/* One packet to transmit is:
    9 bits addr | 3 bits data | 1 sync bit

    In current application 9 bits of address are predefined bits according to schematic.
    3 bits of data are connected to microcontroller. */
#define PT2262_ADDRESS_PINS_IN_USED 0
#define PT2262_DATA_PINS_IN_USED 3

#define PT2262_DATA_BIT_0_ID 0
#define PT2262_DATA_BIT_0_PORT (uint32_t)RF_D11_GPIO_Port
#define PT2262_DATA_BIT_0_PIN (uint32_t)RF_D11_Pin

#define PT2262_DATA_BIT_1_ID 1
#define PT2262_DATA_BIT_1_PORT (uint32_t)RF_D12_GPIO_Port
#define PT2262_DATA_BIT_1_PIN (uint32_t)RF_D12_Pin

#define PT2262_DATA_BIT_2_ID 2
#define PT2262_DATA_BIT_2_PORT (uint32_t)RF_D13_GPIO_Port
#define PT2262_DATA_BIT_2_PIN (uint32_t)RF_D13_Pin

#define pt2262_lib_set_pin(port, pin, state) HAL_GPIO_WritePin((GPIO_TypeDef *)port, (uint16_t)pin, (GPIO_PinState)state)
#if !defined(pt2262_lib_set_pin)
# error "Define pt2262_lib_set_pin() function for the pt2262 lib!"
#endif

#ifdef __cplusplus
}
#endif

#endif /* __PT2262_RF433_TRANSMITTER_CONFIG_H */
